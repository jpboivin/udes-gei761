project("client")
cmake_minimum_required(VERSION 3.0)

set(HEADERS
  "CClient.h"
  "CTlsClient.h")

set(SOURCES
  "CClient.cpp"
  "client.cpp"
  "CTlsClient.cpp")

include_directories(../outilsOpenSsl)
include_directories(${OPENSSL_INCLUDE_DIR})

add_executable(client ${HEADERS} ${SOURCES})
target_link_libraries(client outilsOpenSsl)
target_link_libraries(client ${OPENSSL_LIBRARIES})
