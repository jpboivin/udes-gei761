#if !defined CSERVEUR_H
#define CSERVEUR_H

#if defined(_WIN32)
#   include <winsock2.h>
#else
#   include <unistd.h>
#   include <sys/types.h>
#   include <sys/socket.h>
#   include <netinet/in.h>
#   include <arpa/inet.h>
#endif // _WIN32

#include <string>

using namespace std;

/**
Abstraction d'une connexion TCP, du côté client. Il se connecte à un serveur
paramétrisé et permet de prendre des données sur la ligne de commande et les
envoyer au serveur.
*/
class CServeur
{
public:
    /**
    Constructeur par défaut.
    */
    CServeur();

    /**
    Destructeur.
    */
    virtual ~CServeur();

    /**
    Écoute pour des connexions du client.
    @param addrServeur l'adresse à laquelle se connecter
    @param portServeur le port auquel se connecter
    */
    virtual int attendre(string addrServeur, unsigned int portServeur);

    /**
    Arrêt du serveur, initiant la fermeture de la connexion.
    */
    virtual void arreter();

protected:
    /**
    Attente de réception de données du client et les imprime au stdout.
    */
    virtual void recevoir();

    /**
    Socket descriptor du socket serveur.
    */
    int m_sdServeur;

    /**
    Socket descriptor du socket créé lors de l'appel à accept()
    */
    int m_sdClient;
};

inline
void CServeur::arreter()
{
#if defined(_WIN32)
    closesocket(m_sdClient);
    closesocket(m_sdServeur);
#else
    close(m_sdClient);
    close(m_sdServeur);
#endif
}

#endif // CSERVEUR_H
