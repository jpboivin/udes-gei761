// Inlusion des sockets Windows.
#if defined(_WIN32)
#   include <winsock2.h>
#endif // _WIN32

// Classe du serveur TLS.
#include "CTlsServeur.h"

/**
    Point d'entrée pour le processus du serveur.
    @param argc: le nombre d'arguments sur la ligne de commande, incluant le programme.
    @param argv: la liste des arguments.
    @return troujours 0.
*/
int main (int argc, char* argv[])
{
#if defined(_WIN32)
    // Initialisation de la librairie winsock.
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 2), &wsaData);
#endif

    // Démarrage du serveur.
    CTlsServeur serveur;
    serveur.attendre("127.0.0.1", 80);
    serveur.recevoir();
    serveur.arreter();

#if defined(_WIN32)
    // Fermeture de la librairie winsock.
    WSACleanup();
#endif

    return 0;
}
